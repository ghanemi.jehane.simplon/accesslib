import { faSwimmingPool, faHotTub, faWifi, faTv, faCar, faSnowflake, faShower, faDoorOpen, faSlash } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";  
import React from 'react'

function Page6() {
    return (
        <div>
            <div className="flex-col grid md:grid-cols-2 md:h-screen ">
            <div className="bg-blue-400">
                <p className=" my-20 md:mt-60 text-white text-6xl text-center">Indiquez aux voyageurs quels sont les équipements de votre logement</p>
            </div>

            <div className="space-y-7 px-10 flex flex-col justify-center">
            <h1 className="font-medium text-2xl">Possédez-vous des équipements hors du commun ?</h1>
            <div className="grid grid-cols-3 space-x-3">

                <div className="flex flex-col items-center hover:border-black border-2 py-10 cursor-pointer rounded-lg"> 
                    <FontAwesomeIcon icon={faSwimmingPool} style={{width:'80px'}}/>
                    <p>Piscine</p>
                </div>

                <div className="flex flex-col items-center hover:border-black border-2 py-10 cursor-pointer rounded-lg"> 
                    <FontAwesomeIcon icon={faHotTub} style={{width:'80px'}}/>
                    <p>Jacuzzi</p>
                </div>

                <div className="flex flex-col items-center hover:border-black border-2 py-10 cursor-pointer rounded-lg"> 
                    <FontAwesomeIcon icon={faWifi} style={{width:'80px'}}/>
                    <p>Wifi</p>
                </div>

                
                
                

            </div>
            <div className="grid grid-cols-3 space-x-3">

                <div className="flex flex-col items-center hover:border-black border-2 py-10 cursor-pointer rounded-lg"> 
                    <FontAwesomeIcon icon={faTv} style={{width:'80px'}}/>
                    <p>Télévision</p>
                </div>

                <div className="flex flex-col items-center hover:border-black border-2 py-10 cursor-pointer rounded-lg"> 
                    <FontAwesomeIcon icon={faCar} style={{width:'80px'}}/>
                    <p>Parking gratuit</p>
                </div>

                <div className="flex flex-col items-center hover:border-black border-2 py-10 cursor-pointer rounded-lg"> 
                    <FontAwesomeIcon icon={faSnowflake} style={{width:'80px'}}/>
                    <p>Climatisation</p>
                </div>

                
                
                

            </div>
            <h1 className="font-medium text-2xl">Sélectionnez les équipements d'accessibilité</h1>
            <div className="grid grid-cols-3 space-x-3">

<div className="flex flex-col items-center hover:border-black border-2 py-10 cursor-pointer rounded-lg"> 
    <FontAwesomeIcon icon={faDoorOpen} style={{width:'80px'}}/>
    <p className="text-center">Entrée des voyageurs de plain-pied</p>
</div>

<div className="flex flex-col items-center hover:border-black border-2 py-10 cursor-pointer rounded-lg"> 
    <FontAwesomeIcon icon={faShower} style={{width:'80px'}}/>
    <p>Douche de plain-pied</p>
</div>

<div className="flex flex-col items-center hover:border-black border-2 py-10 cursor-pointer rounded-lg"> 
    <FontAwesomeIcon icon={faSlash} style={{width:'80px'}}/>
    <p className="text-center">Barre d'appui dans la salle de bain</p>
</div>





</div>

            </div>
            </div>
        </div>
    )
}

export default Page6
