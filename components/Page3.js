import React from 'react'

function Page3() {
    return (
        <div>
               <div className="flex-col grid md:grid-cols-2 md:h-screen ">
            <div className="bg-blue-400">
                <p className=" my-20 md:mt-60 text-white text-6xl text-center">Quel type de logement sera à la disposition des voyageurs ?</p>
            </div>

            <div className="space-y-7 px-10 flex flex-col justify-center">
                <div className="py-8 px-3 border border-gray-200 rounded-lg hover:border-black hover:border-2 active:bg-gray-200">
                    <p className="font-medium text-xl">Un logement entier</p>
                   
                </div>
                <div className=" py-8 px-3 border border-gray-200 rounded-lg hover:border-black hover:border-2 active:bg-gray-200">
                    <p className="font-medium text-xl">Une chambre privée</p>
                    
                </div>
                <div className=" py-8 px-3 border border-gray-200 rounded-lg hover:border-black hover:border-2 active:bg-gray-200">
                    <p className="font-medium text-xl">Une chambre partagée</p>
                    
                </div>
                

            </div>
            </div>
        </div>
    )
}

export default Page3
