import React from 'react'
import { LocationMarkerIcon } from '@heroicons/react/solid'
import MapForm from './MapForm'
function Page5() {
    return (
        <div>
            <div className="flex-col grid md:grid-cols-2 h-screen">
            <div className="bg-blue-400">
                <p className=" my-20 md:mt-60 text-white text-6xl text-center">Où est situé votre logement ?</p>
            </div>

            <div className="flex items-center justify-center 
            s py-2 ">
               <div className="flex z-20 absolute  rounded-full items-center space-x-2 bg-white py-5 px-3 border border-gray-200 focus-within:rounded-lg focus-within:border-2 focus-within:border-black hover:border-black hover:border-2">
               <LocationMarkerIcon className="h-6 w-6" />
                    <input className="outline-none" type="text" placeholder="Saisissez votre adresse"/>
                   
                    
               </div>
           
                
       
                <MapForm/>

            </div>
            </div>
        </div>
    )
}

export default Page5

export async function getServerSideProps() {
    const searchResults = await fetch('https://links.papareact.com/isz')
    .then(
        (res) => res.json())
        

    return {
        props: {
            searchResults,
        },
    };

}