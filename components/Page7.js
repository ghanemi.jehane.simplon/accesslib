import React from 'react'
import Image from 'next/image'
import {ChevronRightIcon } from '@heroicons/react/solid'
import Bookings from './Bookings'
import Header from './Header'
function Page7() {
    return (
        <div>
            <Header/>
            <div>
                <p>Success!</p>
            </div>
        </div>
    )
}

export default Page7

{/* <div className=" flex-col grid md:grid-cols-2 md:h-screen ">
                <div  className="bg-blue-400">
                <p className=" my-20 md:mt-60 text-white text-6xl text-center">Mes annonces</p>

                </div>
                <div className="space-y-4 mt-10">
                <div className="flex cursor-pointer border border-gray-200 rounded-lg py-3 text-center mx-44 justify-between
                hover:border-black hover:border-2 active:bg-gray-200">
                     <div className="relative h-16 w-16 ml-3 ">
                <Image src="https://a0.muscache.com/im/pictures/eadbcbdb-d57d-44d9-9a76-665a7a4d1cd7.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                <p className="flex ml-3 text-md text-center font-medium">Stay at this spacious Edwardian House</p>
                <ChevronRightIcon className="h-7 cursor-pointer"/>
               
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 justify-between
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Maison</p>
                <div className="relative h-16 w-16 mr-3">
                <Image src="https://a0.muscache.com/im/pictures/d1af74db-58eb-46bf-b3f5-e42b6c9892db.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 justify-between
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Annexe</p>
                <div className="relative h-16 w-16 mr-3">
                <Image src="https://a0.muscache.com/im/pictures/32897901-1870-4895-8e23-f08dc0e61750.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 justify-between
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Logement unique</p>
                <div className="relative h-16 w-16 mr-3">
                <Image src="https://a0.muscache.com/im/pictures/7ad56bb1-ed9f-4dcb-a14c-2523da331b44.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 justify-between
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Chambre d'hôtes</p>
                <div className="relative h-16 w-16 mr-3 ">
                <Image src="https://a0.muscache.com/im/pictures/d52fb4e7-39a4-46df-9bf9-67e56d35eeca.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                <div className="flex border border-gray-200 rounded-lg py-3 text-center mx-44 justify-between
                hover:border-black hover:border-2 active:bg-gray-200">
                <p className="ml-4 text-xl font-medium">Boutique-hôtel</p>
                <div className="relative h-16 w-16 mr-3">
                <Image src="https://a0.muscache.com/im/pictures/a2c9ad21-b159-4fd2-b417-d810fb23c6a9.jpg?im_w=240"
                layout='fill'
                className="rounded-lg"/>
                </div>
                
                </div>
                
                
                
            </div>
            </div> */}