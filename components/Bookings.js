import React, { useState } from 'react'
import Header from './Header'
import Head from 'next/head'
import Image from 'next/image'
import { MinusCircleIcon, PlusCircleIcon, XCircleIcon } from '@heroicons/react/solid'
import { CheckCircleIcon } from '@heroicons/react/outline'

function Bookings() {
    const [count, setCount] = useState(1);

    const increment = () => {
        setCount(prev => prev+1);



    }

    const decrement = () => {
        setCount(count-1);
    }
    return (
        <div>
            <Head>
                <title>AirBnb</title>
                <link rel="icon" href="/favicon.ico" />
            </Head>

            {/* Header */}
            <Header />

            <div className=" grid grid-cols-3 my-20 ">
                <section className="space-y-5 ml-10 cursor-pointer">
                    <p className="hover:underline" id="photos"><a href="photos"></a>Photos</p>
                    <p className="hover:underline" id="infos">Informations de base sur le logement</p>
                    <p className="hover:underline" id="equipements">Equipements</p>
                    <p className="hover:underline" id="accessibilite">Accessibilité</p>
                    <p className="hover:underline" id="adresse">Adresse</p>
                    <p className="hover:underline"><a href="#logement"></a>Logement et chambres</p>
                    
                </section>


                <div className=" space-y-5">
                    <section href="photos">
                        <div className="flex justify-between mb-5">
                            <p className="font-medium text-xl">Photos</p>
                            <button className="underline text-sm font-semibold">Modifier</button>
                        </div>
                        <div className="space-y-3 lg:flex lg:space-x-3">
                            <div className="relative  w-40 h-40">
                                <Image
                                    src="https://images.unsplash.com/photo-1523217582562-09d0def993a6?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxzZWFyY2h8MXx8aG91c2V8ZW58MHwyfDB8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
                                    layout="fill" className="rounded-lg cursor-pointer" />
                            </div>
                            <div className="relative  w-40 h-40">
                                <Image
                                    src="https://images.unsplash.com/photo-1616594039964-ae9021a400a0?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=880&q=80"
                                    layout="fill" className="rounded-lg cursor-pointer" />
                            </div>
                            <div className="relative  w-40 h-40">
                                <Image
                                    src="https://images.unsplash.com/photo-1604709177225-055f99402ea3?ixlib=rb-1.2.1&ixid=MnwxMjA3fDB8MHxwaG90by1yZWxhdGVkfDJ8fHxlbnwwfHx8fA%3D%3D&auto=format&fit=crop&w=500&q=60"
                                    layout="fill" className="rounded-lg cursor-pointer" />
                            </div>


                        </div>

                    </section>



                    <div className="divide-y space-y-10 justify-start">
                        <p className="font-medium text-xl mt-28">Informations de base sur le logement</p>

                        <section href="infos">
                            <div className="flex justify-between">
                                <p className="text-lg">Titre de l'annonce</p>
                                <button className="underline font-semibold text-sm">Modifier</button>
                            </div>
                            <p className="text-gray-500 text-sm">Joli appartement Lyon 3ème</p>
                        </section>

                        <section>
                            <div className="flex justify-between">
                                <p className="text-lg">Description du logement</p>
                                <button className="underline font-semibold text-sm">Modifier</button>
                            </div>
                            <p className="text-gray-500 text-sm">Faites une pause et détendez-vous dans cette oasis paisible.</p>
                        </section>

                        <section className="flex justify-between">
                            <p className="text-lg">Nombre de voyageurs</p>
                            <div className="flex items-center justify-center" >
                                <button onClick={decrement}>
                                    <MinusCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                                {count}
                                <button onClick={increment}>
                                    <PlusCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                            </div>
                        </section>





                       
                    </div>

                    <div className="divide-y space-y-10 justify-start">
                        <p className="font-medium text-xl mt-28">Equipements</p>

                       

                        <section className="flex justify-between" href="equipements">
                            <p className="text-lg">Wifi</p>
                            <div className="flex items-center justify-center" >
                                <button>
                                    <CheckCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                                
                                <button>
                                    <XCircleIcon className="w-7 h-7 text-red-400" />
                                </button>
                            </div>
                        </section>
                        <section className="flex justify-between">
                            <p className="text-lg">Climatisation</p>
                            <div className="flex items-center justify-center" >
                                <button>
                                    <CheckCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                                
                                <button>
                                    <XCircleIcon className="w-7 h-7 text-red-400" />
                                </button>
                            </div>
                        </section>
                        <section className="flex justify-between">
                            <p className="text-lg">Piscine</p>
                            <div className="flex items-center justify-center" >
                                <button>
                                    <CheckCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                              
                                <button>
                                    <XCircleIcon className="w-7 h-7 text-red-400" />
                                </button>
                            </div>
                        </section>
                        <section className="flex justify-between">
                            <p className="text-lg">Jacuzzi</p>
                            <div className="flex items-center justify-center" >
                                <button>
                                    <CheckCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                              
                                <button>
                                    <XCircleIcon className="w-7 h-7 text-red-400" />
                                </button>
                            </div>
                        </section>





                       
                    </div>

                    <div className="divide-y space-y-10 justify-start">
                        <p className="font-medium text-xl mt-28">Accessibilité</p>

                       

                        <section className="flex justify-between" href="accessibilite">
                            <p className="text-lg">Place de parking accessible</p>
                            <div className="flex items-center justify-center" >
                                <button>
                                    <CheckCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                               
                                <button>
                                    <XCircleIcon className="w-7 h-7 text-red-400" />
                                </button>
                            </div>
                        </section>
                        <section className="flex justify-between">
                            <p className="text-lg">Entrée de plain-pied pour les voyageurs</p>
                            <div className="flex items-center justify-center" >
                                <button>
                                    <CheckCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                             
                                <button>
                                    <XCircleIcon className="w-7 h-7 text-red-400" />
                                </button>
                            </div>
                        </section>
                        <section className="flex justify-between">
                            <p className="text-lg">Lève-personne pour la piscine ou le jacuzzi
</p>
                            <div className="flex items-center justify-center" >
                                <button>
                                    <CheckCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                               
                                <button>
                                    <XCircleIcon className="w-7 h-7 text-red-400" />
                                </button>
                            </div>
                        </section>
                        <section className="flex justify-between">
                            <p className="text-lg">Lève-personne mobile ou fixé au plafond
</p>
                            <div className="flex items-center justify-center" >
                                <button>
                                    <CheckCircleIcon className="w-7 h-7 text-blue-400" />
                                </button>
                               
                                <button>
                                    <XCircleIcon className="w-7 h-7 text-red-400" />
                                </button>
                            </div>
                        </section>





                       
                    </div>


                    <div className="divide-y space-y-10 justify-start">
                        <p className="font-medium text-xl mt-28">Adresse</p>

                        <section href="adresse">
                            <div className="flex justify-between">
                                <p className="text-lg">Adresse</p>
                                <button className="underline font-semibold text-sm">Modifier</button>
                            </div>
                            <p className="text-gray-500 text-sm">24 rue de la Vilette 69003 Lyon</p>
                        </section>

                        <section>
                            <div className="flex justify-between">
                                <p className="text-lg">Description du quartier</p>
                                <button className="underline font-semibold text-sm">Modifier</button>
                            </div>
                            <p className="text-gray-500 text-sm">Non défini</p>
                        </section>
                    </div>


                    <div className="divide-y space-y-10 justify-start">
                        <p className="font-medium text-xl mt-28" >Logement et chambres</p>

                        <section id="logement">
                            <div className="flex justify-between">
                                <p className="text-lg">Type de logement</p>
                                <button className="underline font-semibold text-sm">Modifier</button>
                            </div>
                            <p className="text-gray-500 text-sm">Appartement</p>
                            <p className="text-gray-500 text-sm">Type d'annonce: Logement entier</p>

                        </section>

                        <section>
                            <div className="flex justify-between">
                                <p className="text-lg">Pièces et espaces</p>
                                <button className="underline font-semibold text-sm">Modifier</button>
                            </div>
                            <p className="text-gray-500 text-sm">Chambres: 2</p>
                            <p className="text-gray-500 text-sm">Lits: 4</p>
                            <p className="text-gray-500 text-sm">Salle de bain: 1</p>
                        </section>
                    </div>

                </div>


              


            </div>
        </div>
    )
}

export default Bookings
