import React from 'react'

function Page2() {
    return (
        <div>
            <div className="flex-col grid md:grid-cols-2 md:h-screen ">
            <div className="bg-blue-400">
                <p className="my-20 md:mt-60 text-white text-6xl text-center">Parmi les propositions suivantes, laquelle décrit le mieux votre logement ?</p>
            </div>

            <div className="space-y-7 px-10 mt-10">
                <div className="py-8 px-3 border border-gray-200 rounded-lg hover:border-black hover:border-2 active:bg-gray-200">
                    <p className="font-medium text-xl">Guest house</p>
                    <p className="text-gray-500 text-sm">Une remise ou une dépendance partageant le même terrain que le bâtiment principal.</p>
                </div>
                <div className=" py-8 px-3 border border-gray-200 rounded-lg hover:border-black hover:border-2 active:bg-gray-200">
                    <p className="font-medium text-xl">Suite</p>
                    <p className="text-gray-500 text-sm">Un logement avec une entrée privée à l'intérieur ou attenante à une autre structure plus grande.</p>
                </div>
                <div className=" py-8 px-3 border border-gray-200 rounded-lg hover:border-black hover:border-2 active:bg-gray-200">
                    <p className="font-medium text-xl">Gîte à la ferme</p>
                    <p className="text-gray-500 text-sm">Un logement rural où les voyageurs peuvent passer du temps avec les animaux, pratiquer la randonnée, ou l'artisanat.</p>
                </div>
                <div className=" py-8 px-3 border border-gray-200 rounded-lg hover:border-black hover:border-2 active:bg-gray-200">
                    <p className="font-medium text-xl">Logement de vacances</p>
                    <p className="text-gray-500 text-sm">Une location meublée avec une cuisine et une salle de bain pouvant offrir certains services aux voyageurs, comme un service de réception.</p>
                </div>

            </div>
            </div>
        </div>
    )
}

export default Page2
