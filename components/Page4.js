import React, { useState } from 'react'
import {PlusCircleIcon, MinusCircleIcon} from '@heroicons/react/outline'
function Page4() {
    //Voyageurs

    const [count, setCount] = useState(1);

    const increment = () => {
        setCount(prev => prev+1);

    }

    const decrement = () => {
        setCount(count-1);
    }

    //Lits

    const [bedCount, setBedCount] = useState(0);

    const incrementBed = () => {
        setBedCount(i => i+1);

    }

    const decrementBed = () => {
        setBedCount(bedCount-1);
    }


    // Salles de bain
    const [bainCount, setBainCount] = useState(0);

    const incrementBain = () => {
        setBainCount(item => item+1);

    }

    const decrementBain = () => {
        setBainCount(bainCount-1);
    }
    return (
        <div>
                    <div className="flex-col grid md:grid-cols-2 md:h-screen ">
            <div className="bg-blue-400 ">
                <p className="my-20 md:mt-60 text-white text-6xl text-center">Combien de voyageurs souhaitez-vous accueillir ?</p>
            </div>

            <div className="mt-24 mb-56 ml-44 space-y-7 px-10 flex flex-col justify-center w-1/2">
                <div className="flex justify-between">
                    <p className="font-medium text-2xl">Voyageurs</p>
                    <div className="flex items-center justify-center" >
                    <button onClick={decrement}>
                        <MinusCircleIcon className="w-7 h-7"/>
                    </button>
                    {count}
                    <button onClick={increment}>
                        <PlusCircleIcon className="w-7 h-7"/>
                    </button>
                    </div>
                </div>


                <div className=" flex justify-between">
                    <p className="font-medium text-2xl">Lits</p>
                    <div className="flex items-center justify-center">
                    <button onClick={decrementBed}>
                        <MinusCircleIcon className="w-7 h-7"/>
                    </button>
                    {bedCount}
                    <button onClick={incrementBed}>
                        <PlusCircleIcon className="w-7 h-7"/>
                    </button>
                    </div>
                   
                </div>
                <div className=" flex  justify-between">
                    <p className="font-medium text-2xl">Salles de bain</p>
                    <div className="flex items-center justify-center">
                    <button onClick={decrementBain}>
                        <MinusCircleIcon className="w-7 h-7"/>
                    </button>
                    {bainCount}
                    <button onClick={incrementBain}>
                        <PlusCircleIcon className="w-7 h-7"/>
                    </button>
                    </div>
                </div>
                

            </div>
            </div>
        </div>
    )
}

export default Page4
