import React, { useState } from 'react'
import Page1 from '../components/Page1';
import Page2 from '../components/Page2';
import Page3 from '../components/Page3';
import Page4 from '../components/Page4';
import Page5 from '../components/Page5';
import Page6 from '../components/Page6';
import { ProgressBar } from "react-step-progress-bar";
import "react-step-progress-bar/styles.css";
import {ProgressBarComponent} from '@syncfusion/ej2-react-progressbar';
import Page7 from '../components/Page7';



function host() {
    const [page, setPage] = useState(1);

    function goNextPage() {
        setPage(page => page + 1);
    }

    function goPreviousPage() {
        setPage(page => page - 1)
    }



    return (
        <div>
            <div>
                {page === 1 && <Page1 />}
                {page === 2 && <Page2 />}
                {page === 3 && <Page3 />}
                {page === 4 && <Page4 />}
                {page === 5 && <Page5 />}
                {page === 6 && <Page6 />}
                {page === 7 && <Page7 />}
            </div>
            <div className="flex flex-col">

                <div className="mt-2 w-full lg:w-1/2 right-0 absolute">
                    {/* <ProgressBar
                        
                        filledBackground=""
                        

                    /> */}
                    {/* <progress max="7" width={400}
                        value={page}  /> */}
                 <ProgressBarComponent
                 id="linear"
                 type='Linear'
                 height='60'
                 value={page}
                 maximum={7}/>
                </div>

                <div className="flex mt-10 lg:w-1/2 md:w-1/2 justify-between md:right-0 md:absolute p-2">
                    <button className="underline font-medium text-gray-800 hover:text-black" onClick={goPreviousPage}>Retour</button>
                    <button className="bg-gray-800 rounded-lg p-5 text-white font-medium content-end" onClick={goNextPage}>Suivant</button>

                </div>


            </div>
        </div>
    )
}

export default host
